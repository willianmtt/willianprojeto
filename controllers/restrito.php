<?
    require "../uteis.php";

    $user = new Usuario();
    
    if(isset($_POST['usuario']) && isset($_POST['senha'])){

        $user->usuario = $_POST['usuario'];
        $user->senha = $_POST['senha'];

        // echo $user->login();

        if($user->login()){
            header("Location: ".$url_site."?page=inicio");
        } else {
            header("Location: ".$url_site."login.php?msg=Login/senha-incorreta.");
        }
    }
?>