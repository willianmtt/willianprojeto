<form action="" method="post" class="formCadastro">
    <div class="form-group col-6">
        Nome
        <input type="text" name="nome_moradores" class="form-control" value="<?= $moradores['nome_moradores'] ?>" required">
        CPF
        <input type="text" data-mask="000.000.000-00" name="cpf" class="form-control" value="<?= $moradores['cpf'] ?>" required">
        E-mail
        <input type="text" name="email" class="form-control" value="<?= $moradores['email'] ?>" required">
        Telefone
        <input data-mask="(00) 0000-0000" type="text" name="telefone" class="form-control" value="<?= $moradores['telefone'] ?>"">
        Condomínio
        <select name="from_condominio" class="form-control fromCondominio">
        <option selected>Selecione...</option>
        <? foreach ($resultCond as $key => $value) { ?>
            <option value="<?=$value['id']?>"<?=($value['id'] == $moradores['from_condominio'] ? 'selected' : '')?>><?=$value['nome_condominio'] ?></option>
        <? } ?>
        </select>
        Bloco
        <select name="from_bloco" class="form-control fromBloco">
            <?
            if($_GET['id']){
                $blocos = $morador->getBlocoFromCond($moradores['from_condominio']);
                foreach($blocos['resultSet'] as $bloco){
            ?>
            <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $moradores['from_bloco'] ? 'selected' : '')?>><?=$bloco['numero_bloco']?></option>
            <?} }?>
        </select>
        Unidade
        <select name="from_unidade" class="form-control fromUnidade">
        <?
            if($_GET['id']){
                $unidades = $morador->getUnidadesFromBloco($moradores['from_bloco']);
                foreach($unidades['resultSet'] as $unidade){
            ?>
            <option value="<?=$unidade['id']?>"<?=($unidade['id'] == $moradores['from_unidade'] ? 'selected' : '')?>><?=$unidade['numero_unidade']?></option>
            <?} }?>
        </select>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
        <button type="submit" class="btn btn-primary mt-3 botaozin">Enviar</button>
    </div>
</form>