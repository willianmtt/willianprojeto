<form action="" method="post" class="formConselho">
    <div class="form-group col-6">
        Nome
        <input type="text" name="nome_conselho" class="form-control" value="<?=  $conselho['nome_conselho'] ?>" required">
        CPF
        <input type="text" name="cpf" data-mask="000.000.000-00"class="form-control" value="<?= $conselho['cpf'] ?>" required">
        E-Mail
        <input type="text" name="email" class="form-control" value="<?= $conselho['conselho'] ?>" required">
        Telefone
        <input type="text" name="telefone" data-mask="(00) 0000-0000" class="form-control" value="<?= $conselho['telefone'] ?>" required">
        Função
        <select name="funcao" class="form-control">
        <? foreach($funcoes as $key => $value){?>
            <option value="<?=$key?>"><?=$value?></option>
        <?}?>
        </select>
        Condomínio
        <select name="from_condominio" class="form-control">
        <option selected>Selecione...</option>
        <? foreach ($resultCond as $key => $value) { ?>
            <option value="<?=$value['id']?>"<?=($value['id'] == $conselho['from_condominio'] ? 'selected' : '')?>><?=$value['nome_condominio'] ?></option>
        <? } ?>
        </select>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    <button type="submit" class="btn btn-primary mt-3 botaozin">Enviar</button>
    </div>
</form>