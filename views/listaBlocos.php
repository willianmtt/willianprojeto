<div class="row">
    <div class="col-12">
        <table class="table" id="listaBlocos">
            <tr>
                <th scope="col">Bloco</th>
                <th scope="col">Qtd. Andares</th>
                <th scope="col">Unidade</th>
                <th scope="col">Condomínio</th>
                <th><a href="index.php?page=bloco" class="btn btn-primary">Registrar</a></th>
            </tr>
            <?
            foreach ($result['resultSet'] as $key => $valor) {
            ?>

                <tr data-id="<?= $valor['id'] ?>">
                    <td><?= $valor['numero_bloco'] ?></td>
                    <td><?= $valor['qt_andares'] ?></td>
                    <td><?= $valor['unidade'] ?></td>
                    <td><?= $valor['nome_condominio'] ?></td>
                    <td><a href="<?=$url_site?>bloco/id/<?= $valor['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?= $valor['id'] ?>" class="removerBlocos"><i class="bi bi-trash"></i></a></td>
                <tr>
                <? } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao ?>
        </div>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['bloco'][$_GET['deletar']]);
    header('Location: index.php?page=listaBlocos');
};
?>