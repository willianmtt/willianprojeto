<div class="row">
    <div class="col-12">
        <tr>
            <td colspan="5">
                <form class="form-inline mt-2 mb-2" method="GET" id="filtro">
                    <input type="hidden" name="page" value="condominos">
                    <input class="form-control mr-sm-2 termo1" type="search" placeholder="Pesquisar" aria-label="Search" name="b[nome_moradores]">
                    <select name="b[from_condominio]" class="form-control termo2">
                        <option value="">Condomínios</option>
                        <?
                        $condominio = new Condominio();
                        $resultCond = $condominio->getCondominios();
                        foreach ($resultCond['resultSet'] as $condominios) {
                            echo '<option value="' . $condominios['id'] . '">' . $condominios['nome_condominio'] . '</option>';
                        } ?>
                    </select>
                    <button class="btn btn-outline-primary my-2 my-sm-0 ml-2" disabled type="submit">Buscar</button>
                    <a class="btn btn-outline-danger ml-2" href="<?=$url_site?>condominos">Limpar</a>
                </form>
            </td>
        </tr>
        <table class="table" id="listaClientes">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">CPF</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col">Condomínio</th>
                <th scope="col">Bloco</th>
                <th scope="col">Unidade</th>
                <th scope="col">Data cadastro</th>
                <th scope="col">Data atualizado</th>
                <th><a href="index.php?page=cadastro" class="btn btn-primary">Registrar</a></th>
            </tr>
            <? foreach ($result['resultSet'] as $key => $valor) { ?>
                <tr data-id="<?= $key ?>">
                    <td><?= $valor['nome_moradores'] ?></td>
                    <td><?= $valor['cpf'] ?></td>
                    <td><?= $valor['email'] ?></td>
                    <td><?= $valor['telefone'] ?></td>
                    <td><?= $valor['nome_condominio'] ?></td>
                    <td><?= $valor['numero_bloco'] ?></td>
                    <td><?= $valor['numero_unidade'] ?></td>
                    <td><?= dateFormat($valor['data_cadastro']) ?></td>
                    <td><?= dateFormat($valor['data_update']) ?></td>
                    <td><a href="<?=$url_site?>cadastro/id/<?= $valor['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?= $valor['id'] ?>" class="removerCliente"><i class="bi bi-trash"></i></a></td>
                <tr>
                <? } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao ?>
        </div>
    </div>
</div>

<?
// echo '<pre>';
// print_r($_SESSION['cadastro']);
?>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['cadastro'][$_GET['deletar']]);
    header('Location: index.php?page=condominos');
};
?>