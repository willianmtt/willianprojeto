<div class="row">
    <div class="col-12">
        <table class="table" id="listaAdms">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">CNPJ</th>
                <th><a href="index.php?page=administradora" class="btn btn-primary">Registrar</a></th>
            </tr>
            <?
            foreach ($result['resultSet'] as $key => $valor) {
            ?>
                <tr data-id="<?= $valor['id'] ?>">
                    <td><?= $valor['nome_adm'] ?></td>
                    <td><?= $valor['cnpj'] ?></td>
                    <td><a href="<?=$url_site?>administradora/id/<?= $valor['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?= $valor['id'] ?>" class="removerAdm"><i class="bi bi-trash"></i></a></td>
                <tr>
                <? } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao ?>
        </div>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['administradora'][$_GET['deletar']]);
    header('Location: index.php?page=listaAdministradora');
};
?>