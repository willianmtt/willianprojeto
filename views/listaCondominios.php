<div class="row">
    <div class="col-12">
        <table class="table" id="listaCondominios">
        <tr>
            <td colspan="5">
                <form class="form-inline mt-2 mb-2" action="index.php" method="GET">
                    <input type="hidden" name="page" value="listaCondominios">
                    <input class="form-control mr-sm-2 termo1" type="search" placeholder="Pesquisar" aria-label="Search" name="b[nome_condominios]">
                    <select name="b[from_condominio]" class="form-control termo2">
                        <option value="">Condomínios</option>
                        <?
                        $condominio = new Condominio();
                        $resultCond = $condominio->getCondominios();
                        foreach ($resultCond['resultSet'] as $condominios) {
                            echo '<option value="' . $condominios['id'] . '">' . $condominios['nome_condominio'] . '</option>';
                        } ?>
                    </select>
                    <button class="btn btn-outline-primary my-2 my-sm-0 ml-2" disabled type="submit">Buscar</button>
                    <a class="btn btn-outline-danger ml-2" href="index.php?page=listaCondominios">Limpar</a>
                </form>
            </td>
        </tr>
            <tr>
                <th scope="col">Condomínio</th>
                <th scope="col">Qtd. blocos</th>
                <th scope="col">Logradouro</th>
                <th scope="col">Número</th>
                <th scope="col">Bairro</th>
                <th scope="col">Cidade</th>
                <th scope="col">Estado</th>
                <th scope="col">CEP</th>
                <th scope="col">Conselho</th>
                <th scope="col">Adm.</th>
                <th><a href="index.php?page=condominio" class="btn btn-primary">Registrar</a></th>
            </tr>
            <?
            foreach ($result['resultSet'] as $key => $valor) {
            ?>
                <tr data-id="<?= $valor['id'] ?>">
                    <td><?= $valor['nome_condominio'] ?></td>
                    <td><?= $valor['qt_blocos'] ?></td>
                    <td><?= $valor['logradouro'] ?></td>
                    <td><?= $valor['numero'] ?></td>
                    <td><?= $valor['bairro'] ?></td>
                    <td><?= $valor['cidade'] ?></td>
                    <td><?= $valor['estado'] ?></td>
                    <td><?= $valor['cep'] ?></td>
                    <td><?= $valor['nome_conselho'] ?></td>
                    <td><?= $valor['nome_adm'] ?></td>
                    <td><a href="<?=$url_site?>condominio/id/<?= $valor['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?= $valor['id'] ?>" class="removerCondominios"><i class="bi bi-trash"></i></a></td>
                <tr>

                <? } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao ?>
        </div>
    </div>
</div>
<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['condominio'][$_GET['deletar']]);
    header('Location: index.php?page=listaCondominios');
};
?>