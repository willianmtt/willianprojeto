<div class="row" id="listaUsuarios">
    <div class="col-12">
        <table class="table" id="listaCadUsuario">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Usuario</th>
            </tr>
            <?
            foreach ($result['resultSet'] as $key => $valor) {
            ?>
                <tr data-id="<?= $valor['id'] ?>">
                    <td><?= $valor['nome_user'] ?></td>
                    <td><?= $valor['usuario'] ?></td>
                    <td><a href="<?=$url_site?>cadUsuario/id/<?= $valor['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?= $valor['id'] ?>" class="removerUsuario"><i class="bi bi-trash"></i></a></td>
                <tr>

                <? } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao ?>
        </div>
    </div>
</div>
<?
// echo '<pre>';
// print_r($_SESSION['condominio'])
?>
<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['condominio'][$_GET['deletar']]);
    header('Location: index.php?page=listaCadUsuario');
};
?>