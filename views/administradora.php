<form action="" method="post" class="formAdm">
    <div class="form-group col-6">
        Nome
        <input type="text" name="nome_adm" class="form-control" value="<?= $adm['nome_adm'] ?>" required">
        CNPJ
        <input type="text" data-mask="00.000.000/0000-00" name="cnpj" class="form-control" value="<?= $adm['cnpj'] ?>" required">
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    <button type="submit" class="btn btn-primary mt-3 botaozin">Enviar</button>
    </div>
</form>