<form action="" method="post" class="formCondominio">
    <div class="form-group col-6">
        Nome do condomínio
        <input type="text" name="nome_condominio" class="form-control" value="<?= $condominio['nome_condominio'] ?>" required>
        Quantidade de blocos
        <input type="text" name="qt_blocos" class="form-control" value="<?= $condominio['qt_blocos'] ?>" required>
        Logradouro
        <input type="text" name="logradouro" class="form-control" value="<?= $condominio['logradouro'] ?>" required>
        Número
        <input type="text" name="numero" class="form-control" value="<?= $condominio['numero'] ?>">
        Bairro
        <input type="text" name="bairro" class="form-control" value="<?= $condominio['bairro'] ?>">
        Cidade
        <input type="text" name="cidade" class="form-control" value="<?= $condominio['cidade'] ?>">
        Estado
        <select name="estado" class="form-control">
        <?foreach($estados as $sigla => $uf){?>
            <option value="<?=$sigla?>"><?=$uf ?></option>
        <?}?>
        </select> 
        CEP
        <input data-mask="00000-000" type="text" name="cep" class="form-control" value="<?= $condominio['cep'] ?>">
        Conselho
        <select name="from_conselho" class="form-control">
        <option selected>Selecione...</option>
        <? foreach ($resultCons as $key => $value) { ?>
            <option value="<?=$value['id']?>"<?=($value['id'] == $condominio['from_conselho'] ? 'selected' : '')?>><?=$value['nome_conselho'] ?></option>
        <? } ?>
        </select>
        Administradora
        <select name="from_administradora" class="form-control">
        <option selected>Selecione...</option>
        <? foreach ($resultAdm as $key => $value) { ?>
            <option value="<?=$value['id']?>"<?=($value['id'] == $condominio['from_administradora'] ? 'selected' : '')?>><?=$value['nome_adm'] ?></option>
        <? } ?>
        </select>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    <button type="submit" class="btn btn-primary mt-3 botaozin">Enviar</button>
    </div>
</form>