<div class="row">
    <div class="col-12" >
        <table class="table" id="listaUnidade">
            <tr >
                <th scope="col">Número unidade</th>
                <th scope="col">Metragem</th>
                <th scope="col">Vagas garagem</th>
                <th scope="col">Bloco</th>
                <th scope="col">Condomínio</th>
                <th><a href="index.php?page=unidades" class="btn btn-primary">Registrar</a></th>
            </tr>
            <?
            foreach ($result['resultSet'] as $key => $valor) {
            ?>
                    
                    <tr data-id="<?=$valor['id']?>">
                    <td><?= $valor['numero_unidade'] ?></td>
                    <td><?= $valor['metragem'] ?></td>
                    <td><?= $valor['vagas_garagem'] ?></td>
                    <td><?= $valor['numero_bloco'] ?></td>
                    <td><?= $valor['nome_condominio'] ?></td>
                    <td><a href="<?=$url_site?>unidades/id/<?=$valor['id']?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?=$valor['id']?>" class="removerUnidades"><i class="bi bi-trash"></i></a></td>
                    <tr>
            <? } ?>
            <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao?>
        </div>
    </div>
</div>

<?
if(!empty($_GET['deletar'])){
    unset($_SESSION['unidades'][$_GET['deletar']]);
    header('Location: index.php?page=listaUnidades');
};
?>