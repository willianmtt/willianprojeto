<form action="" method="post" class="formUnidades">
    <div class="form-group col-6">
        Número da unidade
        <input type="text" name="numero_unidade" class="form-control" value="<?= $unidade['numero_unidade'] ?>" required">
        Metragem
        <input type="text" name="metragem" class="form-control" value="<?= $unidade['metragem'] ?>" required">
        Vagas da garagem
        <input type="text" name="vagas_garagem" class="form-control" value="<?= $unidade['vagas_garagem'] ?>" required">
        Condomínio
        <select name="from_condominio" class="form-control fromCondominio">
        <option selected>Selecione...</option>
        <? foreach ($resultCond as $key => $value) { ?>
            <option value="<?=$value['id']?>"<?=($value['id'] == $unidade['from_condominio'] ? 'selected' : '')?>><?=$value['nome_condominio'] ?></option>
        <? } ?>
        </select>
        Bloco
        <select name="from_bloco" class="form-control fromBloco">
            <?
            if($_GET['id']){
                $blocos = $unidades->getBlocoFromCond($unidade['from_condominio']);
                foreach($blocos['resultSet'] as $bloco){
            ?>
            <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $unidade['from_bloco'] ? 'selected' : '')?>><?=$bloco['numero_bloco']?></option>
            <?} }?>
        </select>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn btn-primary mt-3 botaozin">Enviar</button>
    </div>
</form>