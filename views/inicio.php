<div class="row">
    <div class="card col-sm-6" style="width: 99rem;">
        <div class="card-body">
            <h5 class="card-title h3">Número de moradores p/ condomínio</h5>
            <?
            $moradores = new UltimasAdm();
            $morador = $moradores->getMoradoresCond();
            foreach ($morador['resultSet'] as $value) {
            ?>
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <?= $value['nome_condominio'] ?>

                        <span class="badge badge-primary badge-pill"><?= $value['totalMoradores'] ?></span>
                    </li>
                </ul>
            <? } ?>
        </div>
    </div>
    <div class="card col-sm-6" style="width: 99rem;">
        <div class="card-body">
            <h5 class="card-title h3">Últimas 5 administradoras</h5>
            <ul>
                <?
                $adms = new UltimasAdm();
                $adm = $adms->getUltimasAdm();
                foreach ($adm['resultSet'] as $value) {
                ?>
                    <li class="list-group-item" style="margin-left:-45px"><?= $value['nome_adm'] ?></li>
                <? } ?>
            </ul>
        </div>
    </div>
</div>

<div class="row mt-5 text-center card-group">

    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Administradoras</h5>
            <?
            $adm = new Administradora();
            $result = $adm->getAdm()
            ?>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
            <a href="index.php?page=listaAdministradora" class="btn btn-primary">Ir para</a>
        </div>
    </div>

    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Condomínios</h5>
            <?
            $condominio = new Condominio();
            $result = $condominio->getCondominios()
            ?>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
            <a href="index.php?page=listaCondominios" class="btn btn-primary">Ir para</a>
        </div>
    </div>

    <div class="card ml-2" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Moradores</h5>
            <?
            $condomino = new Cadastro();
            $result = $condomino->getCondominos();
            ?>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
            <a href="index.php?page=condominos" class="btn btn-primary">Ir para</a>
        </div>
    </div>

    <div class="card ml-2" style="width: 18rem;">
        <div class="card-body">
            <?
            $unidade = new Unidade();
            $result = $unidade->getUnidades();
            ?>
            <h5 class="card-title">Unidades</h5>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
            <a href="index.php?page=listaUnidades" class="btn btn-primary">Ir para</a>
        </div>
    </div>

    <div class="card ml-2" style="width: 18rem;">
        <div class="card-body">
            <?
            $bloco = new Bloco();
            $result = $bloco->getBlocos();
            ?>
            <h5 class="card-title">Blocos</h5>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
            <a href="index.php?page=listaBlocos" class="btn btn-primary">Ir para</a>
        </div>
    </div>

</div>