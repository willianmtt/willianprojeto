<form action="" method="post" class="formBloco">
    <div class="form-group col-6">
        Bloco
        <input type="text" name="numero_bloco" class="form-control" value="<?= $bloco['numero_bloco'] ?>" required">
        Qtd. Andares
        <input type="text" name="qt_andares" class="form-control" value="<?= $bloco['qt_andares'] ?>" required">
        Unidades p/ andar
        <input type="text" name="unidade" class="form-control" value="<?= $bloco['unidades'] ?>" required">
        Condomínio
        <select name="from_condominios" class="form-control">
        <option selected>Selecione...</option>
        <? foreach ($resultCond as $key => $value) { ?>
            <option value="<?=$value['id']?>"<?=($value['nome_condominio'] == $bloco['nome_condominio'] ? 'selected' : '')?>><?=$value['nome_condominio'] ?></option>
        <? } ?>
        </select>
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    <button type="submit" class="btn btn-primary mt-3 botaozin">Enviar</button>
    </div>
</form>