<div class="row">
    <div class="col-12">
        <table class="table" id="listaConselho">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">CPF</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col">Função</th>
                <th scope="col">Condomínio</th>
                <th><a href="index.php?page=conselho" class="btn btn-primary">Registrar</a></th>
            </tr>
            <? foreach ($result['resultSet'] as $key => $valor) { ?>
                <tr data-id="<?= $valor['id'] ?>">
                    <td><?= $valor['nome_conselho'] ?></td>
                    <td><?= $valor['cpf'] ?></td>
                    <td><?= $valor['email'] ?></td>
                    <td><?= $valor['telefone'] ?></td>
                    <td><?= $valor['funcao'] ?></td>
                    <td><?= $valor['nome_condominio'] ?></td>
                    <td><a href="<?=$url_site?>conselho/id/<?= $valor['id'] ?>"><i class="bi bi-pencil-square"></i></a></td>
                    <td><a href="#" data-id="<?= $valor['id'] ?>" class="removerConselho"><i class="bi bi-trash"></i></a></td>
                <tr>
                <? } ?>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="left" class="totalRegistros">Total: <?= $totalRegistros ?></td>
                </tr>
        </table>
        <div class="col-sm-12">
            <?= $paginacao ?>
        </div>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['conselho'][$_GET['deletar']]);
    header('Location: index.php?page=conselho');
};
?>