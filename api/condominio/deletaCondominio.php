<?

use LDAP\Result;

    require "../../uteis.php";

    $condominio = new Condominio();
    $result = $condominio->deletaCondominios($_POST['id']);
    if ($result){

        $totalRegistros = $condominio->getCondominios()['totalResults'];

        $result = array(
            "status" => 'success',
            "totalRegistros"=>($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
            "msg" => "Seu registro foi deletado."
        );
        echo json_encode($result);
    } else {
        $result = array(
            "status" => 'danger',
            "msg" => "O registro não pode ser deletado."
        );
        echo json_encode($result);
    }
?>