<?

use LDAP\Result;

    require "../../uteis.php";

    $bloco = new Bloco();
    $result = $bloco->deletaBlocos($_POST['id']);
    if ($result){

        $totalRegistros = $bloco->getBlocos()['totalResults'];

        $result = array(
            "status" => 'success',
            "totalRegistros"=>($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
            "msg" => "Seu registro foi deletado."
        );
        echo json_encode($result);
    } else {
        $result = array(
            "status" => 'danger',
            "msg" => "O registro não pode ser deletado."
        );
        echo json_encode($result);
    }
?>