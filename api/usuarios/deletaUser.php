<?

use LDAP\Result;

    require "../../uteis.php";

    $usuario = new Usuarios();
    $result = $usuario->deletaUsuarios($_POST['id']);
    if ($result){

        $totalRegistros = $usuario->getUsuarios($id)['totalResults'];

        $result = array(
            "status" => 'success',
            "totalRegistros"=>($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
            "msg" => "Seu registro foi deletado."
        );
        echo json_encode($result);
    } else {
        $result = array(
            "status" => 'danger',
            "msg" => "O registro não pode ser deletado."
        );
        echo json_encode($result);
    }
?>