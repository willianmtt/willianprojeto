<?
session_start();
$localDir = 'willianprojeto/';
$base_url = "http://".$_SERVER['HTTP_HOST'].'/';

$url_site = $base_url.$localDir;

$fullPath = $_SERVER['DOCUMENT_ROOT'].'/';
$fullPath .= $localDir;

// $includes = $fullPath.'includes/';
$models = $fullPath.'models/';
$controllers = $fullPath.'controllers/';
$views = $fullPath.'views/';

require $models."connectDB.class.php";
require $models."restrito.Class.php";
require $models."dao.Class.php";
require $models."ultimasAdm.Class.php";
include $models."condominio.Class.php";
include $models."bloco.Class.php";
include $models."unidade.Class.php";
include $models."cadastro.Class.php";
include $models."conselho.Class.php";
include $models."adm.Class.php";
include $models."cadUsuario.Class.php";

error_reporting(E_ERROR|E_PARSE);

// set_error_handler('trataErros');

$nav = array(
    // 'inicio' => 'Início',
    'Cadastros' => array(
        'cadastro' => 'Morador',
        'condominio' => 'Condomínio',
        'bloco' => 'Bloco',
        'unidades' => 'Unidade',
    ),
    'Listas' => array(
        'condominos' => 'Moradores',
        'listaCondominios' => 'Condomínios',
        'listaBlocos' => 'Blocos',
        'listaUnidades' => 'Unidades',
    ),
);

function dateFormat($d, $tipo = true){
    if(!$d){
        return 'Sem data';
    }
    if($tipo){
    $hora = explode(' ',$d);
    $data = explode('-',$hora[0]);
    return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];
} else {
    $hora = explode(' ',$d);
    $data = explode('/',$hora[0]);
    return $data[2].'-'.$data[1].'-'.$data[0].' '.$hora[1];
    }
}

$estados = array(
    "AC" => "Acre", 
    "AL" => "Alagoas", 
    "AM" => "Amazonas", 
    "AP" => "Amapá",
    "BA" => "Bahia",
    "CE" => "Ceará",
    "DF" => "Distrito Federal",
    "ES" => "Espírito Santo",
    "GO" => "Goiás",
    "MA" => "Maranhão",
    "MT" => "Mato Grosso",
    "MS" => "Mato Grosso do Sul",
    "MG" => "Minas Gerais",
    "PA" => "Pará",
    "PB" => "Paraíba",
    "PR" => "Paraná",
    "PE" => "Pernambuco",
    "PI" => "Piauí",
    "RJ" => "Rio de Janeiro",
    "RN" => "Rio Grande do Norte",
    "RO" => "Rondônia",
    "RS" => "Rio Grande do Sul",
    "RR" => "Roraima",
    "SC" => "Santa Catarina",
    "SE" => "Sergipe",
    "SP" => "São Paulo",
    "TO" => "Tocantins"
);

$funcoes = array(
    "Sindico",
    "Sub-síndico",
    "Conselheiro",
    "Contador"
);

function legivel($var,$width = '250',$height = '400') {
    echo "<pre>";
    if(is_array($var)) {
        print_r($var);
    } else {
        print($var);
    }
    echo "</pre>";
};

function trataUrl($params = array()){
    $url = (isset($_GET['b'])) ? 'busca/' : '';
    foreach($params as $key=>$value){
        $url .= $value.'/';
    }
    return $url;
}

function _md5($str,$num=1){
    $md5='';
    for($i=0;$i < $num;$i++){
        $str = md5($str);
        $md5=$str;
    }
    return $md5;
}

// function trataErros($errorNum, $errorStr, $errorFile, $errorLine){
//     echo $errorNum.' x '.$errorStr. ' x ' .$errorFile.' x '.$errorLine;
// }
?>