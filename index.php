<? include "uteis.php"; 

$user = new Usuario;
if($user->acesso()){
    header('Location: login.php');
}
echo $user->acesso();
if(($_GET['page'] == 'logout')){
    if($user->logout()){
        header('Location: '.$url_site.'login.php');
    }
}
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Projeto Condomínio</title>
</head>

<body>
    <main class="container col-md-12 col-sm-10">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="<?=$url_site?>inicio">
                <img src="<?=$url_site?>img/logo.png" width="40" height="40" class="d-inline-block align-top" alt="">
                myHome
            </a>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Administradora
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>administradora">Cadastrar administradora</a>
                    <a class="dropdown-item" href="<?=$url_site?>listaAdministradora">Listar administradora</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Condomínios
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>condominio">Cadastrar condomínios</a>
                    <a class="dropdown-item" href="<?=$url_site?>listaCondominios">Listar condomínios</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Moradores
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>cadastro">Cadastrar moradores</a>
                    <a class="dropdown-item" href="<?=$url_site?>condominos">Listar moradores</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Unidades
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>unidades">Cadastrar unidades</a>
                    <a class="dropdown-item" href="<?=$url_site?>listaUnidades">Listar unidades</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Blocos
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>bloco">Cadastrar bloco</a>
                    <a class="dropdown-item" href="<?=$url_site?>listaBlocos">Listar blocos</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Conselhos
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>conselho">Cadastrar conselho</a>
                    <a class="dropdown-item" href="<?=$url_site?>listaConselho">Listar conselho</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                    Usuários
                </button>
                <div class="dropdown-menu" aria-labelledby="dropInqu">
                    <a class="dropdown-item" href="<?=$url_site?>cadUsuario">Cadastrar usuário</a>
                    <a class="dropdown-item" href="<?=$url_site?>listaCadUsuario">Listar usuários</a>
                </div>
            </div>
            <?
                $nome = explode(' ',$_SESSION['USUARIO']['nome_user'])
            ?>
                <small>Olá <?=$nome[0];?>, seja bem vindo(a)!</small>
            <div>
                <a href="<?=$url_site?>logout" class="nav-link text-dark ml-2"><i class="bi bi-box-arrow-right" action="logout"></i></a>
            </div>
        </nav>

        <?
        switch ($_GET['page']) {
            case '';
                'inicio';
                require "controllers/inicio.php";
                require "views/inicio.php";
                break;

            default:
                require 'controllers/'.$_GET['page'].'.php';
                require 'views/'.$_GET['page'].'.php';
                break;
        };
        ?>
    </main>
    <footer class="col-12 bg-light text-center text-lg-start" style="bottom:0; position:fixed;">
        <div class="text-center p-3"">
            © 2022 Copyright:
            <a class=" text-dark" href="#">myHome.com</a>
        </div>
    </footer>
    <script> var url_site = '<?=$url_site?>';</script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/app.js?v=<?= rand(0, 9999) ?>"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/jquery.mask.min.js"></script>

</body>

</html>

<?

if (isset($_POST['g'])) {
};
?>