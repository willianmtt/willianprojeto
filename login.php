<? include "uteis.php"; ?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Projeto Condomínio</title>
</head>

<body>
    <main class="container col-md-12 col-sm-10">
       <form action="<?=$url_site?>controllers/restrito.php" method="POST">
            <div class="form-group mt-5 ml-3">
                <div class="col-sm-4">
                    <img src="img/logo.png" width="50%">
                <h2>Efetue seu login:</h2>
                    <label for="nome">Login</label>
                    <input class="form-control" type="text" name="usuario" required>
                </div>
                <div class="col-sm-4">
                    <label for="nome">Senha</label>
                    <input class="form-control" type="password" name="senha" required>
                </div>
                <div class="col-sm-4 form-group">
                    <button class="btn btn-primary mt-3" type="submit">Entrar</button>
                </div>
            </div>
       </form> 
    </main>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/app.js?v=<?= rand(0, 9999) ?>"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        <? if(isset($_GET['msg'])){ ?>
            $(function(){
                myAlert('danger','<?=$_GET['msg']?>','main')
            })
            </script>
        <? } ?>

</body>

</html>

<?

if (isset($_POST['g'])) {
};
?>