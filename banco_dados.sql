-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para projeto
CREATE DATABASE IF NOT EXISTS `projeto` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `projeto`;

-- Copiando estrutura para tabela projeto.administradoras
CREATE TABLE IF NOT EXISTS `administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_adm` varchar(255) NOT NULL DEFAULT '',
  `cnpj` varchar(20) NOT NULL DEFAULT '',
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.administradoras: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `administradoras` DISABLE KEYS */;
INSERT INTO `administradoras` (`id`, `nome_adm`, `cnpj`, `data_cadastro`) VALUES
	(1, 'Admnis', '23.564.453/0001-23', '2022-03-30 13:06:10'),
	(2, 'Lander', '14.124.753/0001-76', '2022-03-30 13:06:42'),
	(3, 'Trent', '13.123.123/0001-23', '2022-03-30 13:05:18'),
	(4, 'Connecta', '61.353.765/0001-08', '2022-04-01 11:28:08'),
	(7, 'Pinheiro X', '81.494.182/0001-04', '2022-04-07 08:07:03'),
	(8, 'NewADM', '14.124.124/1241-24', '2022-04-07 16:56:00');
/*!40000 ALTER TABLE `administradoras` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.blocos
CREATE TABLE IF NOT EXISTS `blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_bloco` int(11) NOT NULL DEFAULT 0,
  `qt_andares` int(11) NOT NULL DEFAULT 0,
  `unidade` int(11) NOT NULL DEFAULT 0,
  `data_cadastro` timestamp NULL DEFAULT current_timestamp(),
  `from_condominios` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_blocos_condominios` (`from_condominios`),
  CONSTRAINT `FK_blocos_condominios` FOREIGN KEY (`from_condominios`) REFERENCES `condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.blocos: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `blocos` DISABLE KEYS */;
INSERT INTO `blocos` (`id`, `numero_bloco`, `qt_andares`, `unidade`, `data_cadastro`, `from_condominios`) VALUES
	(1, 2, 4, 101, '2022-03-29 14:07:50', 1),
	(2, 1, 2, 102, '2022-03-29 14:18:40', 2),
	(3, 3, 2, 103, '2022-03-29 14:22:26', 3),
	(4, 1, 1, 101, '2022-04-01 09:07:18', 1),
	(9, 1, 1, 101, '2022-04-01 13:43:51', 2),
	(10, 1, 1, 0, '2022-04-01 16:05:30', 1),
	(11, 0, 2, 102, '2022-04-01 16:23:19', 1);
/*!40000 ALTER TABLE `blocos` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.condominios
CREATE TABLE IF NOT EXISTS `condominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_condominio` varchar(255) NOT NULL,
  `qt_blocos` int(11) NOT NULL DEFAULT 0,
  `logradouro` varchar(255) DEFAULT '0',
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `cep` varchar(8) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `from_administradora` int(11) NOT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `from_conselho` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_condominios_conselhos` (`from_conselho`),
  KEY `FK_condominios_administradoras` (`from_administradora`),
  CONSTRAINT `FK_condominios_administradoras` FOREIGN KEY (`from_administradora`) REFERENCES `administradoras` (`id`),
  CONSTRAINT `FK_condominios_conselhos` FOREIGN KEY (`from_conselho`) REFERENCES `conselhos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.condominios: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `condominios` DISABLE KEYS */;
INSERT INTO `condominios` (`id`, `nome_condominio`, `qt_blocos`, `logradouro`, `numero`, `bairro`, `cidade`, `cep`, `data_cadastro`, `from_administradora`, `estado`, `from_conselho`) VALUES
	(1, 'Pinheiro IV', 2, 'casa', 102, 'Diamante', 'Blumenau', '89136000', '2022-03-29 14:06:35', 1, 'SC', 1),
	(2, 'Centro Comercial', 1, 'rua 2', 512, 'centro', 'timbo', '14124000', '2022-03-29 14:16:55', 1, 'AC', 1),
	(3, 'Elegance', 2, 'rua 6', 431, 'centro', 'timbo', '43505000', '2022-03-29 14:23:13', 3, 'AC', 2),
	(4, 'Boavida', 1, 'rua 3', 293, 'centro', 'rodeio', '23129000', '2022-03-30 13:07:55', 2, 'AC', 1),
	(5, 'Heroes', 2, 'rua 70', 937, 'gloria', 'rodeio', '94124000', '2022-03-30 13:08:40', 3, 'AC', 2),
	(6, 'FlaCondo', 4, 'rua 200', 400, 'gavea', 'rio de janeiro', '10394321', '2022-03-30 13:09:28', 4, 'AC', 1),
	(7, 'Putfire', 4, 'rua 220', 231, 'botafogo', 'rio de janeiro', '40314101', '2022-03-30 13:09:57', 4, 'AC', 2);
/*!40000 ALTER TABLE `condominios` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.conselhos
CREATE TABLE IF NOT EXISTS `conselhos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_conselho` varchar(255) NOT NULL DEFAULT '0',
  `cpf` varchar(15) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '0',
  `telefone` varchar(255) NOT NULL DEFAULT '0',
  `funcao` enum('Síndico','Sub-Síndico','Contador','Porteiro') NOT NULL,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_conselhos_condominios` (`from_condominio`),
  CONSTRAINT `FK_conselhos_condominios` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.conselhos: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `conselhos` DISABLE KEYS */;
INSERT INTO `conselhos` (`id`, `nome_conselho`, `cpf`, `email`, `telefone`, `funcao`, `from_condominio`, `data_cadastro`) VALUES
	(1, 'Willian', '132.123.123-44', 'willian@email.com', '470000000', '', 1, '2022-03-29 14:09:24'),
	(2, 'Michel', '123.152.125-54', 'michel@email.com', '4799999999', '', 5, '2022-03-29 14:19:08'),
	(3, 'Neyzika', '401.241.424-14', 'neyzika@email.com', '1400009999', '', 6, '2022-03-29 14:24:33');
/*!40000 ALTER TABLE `conselhos` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.lista_convidados
CREATE TABLE IF NOT EXISTS `lista_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(255) NOT NULL DEFAULT '0',
  `from_reserva_salao` int(11) NOT NULL DEFAULT 0,
  `cpf` int(11) NOT NULL DEFAULT 0,
  `celular` varchar(50) NOT NULL DEFAULT '0',
  `from_unidade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__reserva_salao` (`from_reserva_salao`),
  KEY `FK_lista_convidados_unidades` (`from_unidade`),
  CONSTRAINT `FK__reserva_salao` FOREIGN KEY (`from_reserva_salao`) REFERENCES `reserva_salao` (`id`),
  CONSTRAINT `FK_lista_convidados_unidades` FOREIGN KEY (`from_unidade`) REFERENCES `unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.lista_convidados: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `lista_convidados` DISABLE KEYS */;
INSERT INTO `lista_convidados` (`id`, `convidado`, `from_reserva_salao`, `cpf`, `celular`, `from_unidade`) VALUES
	(1, 'Jãozin', 1, 2147483647, '470000000', 1),
	(2, 'Willian', 1, 2147483647, '470000000', 2),
	(3, 'Michel', 1, 2147483647, '470000000', 2),
	(4, 'Neymar', 1, 2147483647, '470000000', 3),
	(5, 'Benzema', 1, 2147483647, '470000000', 2),
	(6, 'Carlin', 1, 2147483647, '470000000', 3),
	(7, 'Maria', 1, 2147483647, '470000000', 2),
	(8, 'Joana', 1, 2147483647, '470000000', 2),
	(9, 'Paulinha', 2, 2147483647, '470000000', 1),
	(10, 'Benzema', 2, 2147483647, '470000000', 1),
	(11, 'Luisa', 2, 2147483647, '470000000', 2),
	(12, 'Benzema', 2, 2147483647, '470000000', 2),
	(13, 'Rodriga', 2, 2147483647, '470000000', 2),
	(14, 'Binária', 2, 2147483647, '470000000', 3),
	(15, 'Mequinha', 2, 2147483647, '470000000', 3),
	(16, 'Ronaldo', 2, 2147483647, '470000000', 2),
	(17, 'Benzema', 3, 2147483647, '470000000', 1),
	(18, 'Luiza', 3, 2147483647, '470000000', 1),
	(19, 'Belzebueno', 3, 2147483647, '470000000', 2),
	(20, 'Pelé', 3, 2147483647, '470000000', 2),
	(21, 'Marco', 3, 2147483647, '470000000', 3),
	(22, 'Jean', 3, 2147483647, '470000000', 3),
	(23, 'Luana', 3, 2147483647, '470000000', 2),
	(24, 'Luciana', 3, 2147483647, '470000000', 2);
/*!40000 ALTER TABLE `lista_convidados` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.moradores
CREATE TABLE IF NOT EXISTS `moradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_moradores` varchar(255) DEFAULT NULL,
  `cpf` varchar(15) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '0',
  `telefone` varchar(50) DEFAULT '0',
  `data_cadastro` timestamp NULL DEFAULT current_timestamp(),
  `from_condominio` int(11) NOT NULL,
  `from_bloco` int(11) NOT NULL,
  `from_unidade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`from_condominio`),
  KEY `chBloco` (`from_bloco`),
  KEY `chUnidade` (`from_unidade`),
  CONSTRAINT `chBloco` FOREIGN KEY (`from_bloco`) REFERENCES `blocos` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`),
  CONSTRAINT `chUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.moradores: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `moradores` DISABLE KEYS */;
INSERT INTO `moradores` (`id`, `nome_moradores`, `cpf`, `email`, `telefone`, `data_cadastro`, `from_condominio`, `from_bloco`, `from_unidade`) VALUES
	(2, 'Michel', '124.245.231-54', 'email@email.com', '4799999999', '2022-03-29 14:20:43', 2, 2, 2),
	(3, 'Will', '414.424.643-51', 'a@a.com', '5900009999', '2022-03-29 14:24:59', 3, 3, 3),
	(6, 'kaka', '401.412.431-55', 'm@m.com', '205025020', '2022-03-30 15:05:39', 6, 3, 3),
	(17, 'Willian', '294', 'wil', '1923829', '2022-04-01 16:10:22', 2, 4, 2),
	(18, 'Willian', 'n', 'cs', 'd', '2022-04-01 16:12:45', 2, 3, 2),
	(22, 'Willian', 'n', 'cs', 'd', '2022-04-04 10:49:25', 1, 1, 1);
/*!40000 ALTER TABLE `moradores` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.pets
CREATE TABLE IF NOT EXISTS `pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pet` varchar(255) NOT NULL DEFAULT '',
  `tipo` enum('Cachorro','Gato','Pássaro') NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `from_morador` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chDogs` (`from_morador`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.pets: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pets` DISABLE KEYS */;
INSERT INTO `pets` (`id`, `nome_pet`, `tipo`, `data_cadastro`, `from_morador`) VALUES
	(1, 'dog', 'Cachorro', '2022-03-30 11:43:14', 6),
	(2, 'neymah', 'Gato', '2022-03-30 11:45:13', 23),
	(3, 'papagaio', 'Cachorro', '2022-03-30 11:46:20', 3);
/*!40000 ALTER TABLE `pets` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.reserva_salao
CREATE TABLE IF NOT EXISTS `reserva_salao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_evento` varchar(255) NOT NULL DEFAULT '0',
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `datahora_evento` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_reserva_salao_unidades` (`from_unidade`),
  CONSTRAINT `FK_reserva_salao_unidades` FOREIGN KEY (`from_unidade`) REFERENCES `unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.reserva_salao: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `reserva_salao` DISABLE KEYS */;
INSERT INTO `reserva_salao` (`id`, `titulo_evento`, `from_unidade`, `datahora_evento`, `data_cadastro`) VALUES
	(1, 'futebol', 1, '2022-05-05 16:00:00', '2022-03-30 16:18:46'),
	(2, 'cinema', 2, '2022-06-30 18:30:00', '2022-03-30 16:19:19'),
	(3, 'corrida', 3, '2022-03-30 20:00:00', '2022-03-30 16:20:08');
/*!40000 ALTER TABLE `reserva_salao` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.unidades
CREATE TABLE IF NOT EXISTS `unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_unidade` int(11) NOT NULL DEFAULT 0,
  `metragem` int(11) NOT NULL DEFAULT 0,
  `vagas_garagem` int(11) NOT NULL DEFAULT 0,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_bloco` int(11) DEFAULT NULL,
  `from_condominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_unidades_blocos` (`from_bloco`),
  KEY `FK_unidades_condominios` (`from_condominio`),
  CONSTRAINT `FK_unidades_blocos` FOREIGN KEY (`from_bloco`) REFERENCES `blocos` (`id`),
  CONSTRAINT `FK_unidades_condominios` FOREIGN KEY (`from_condominio`) REFERENCES `condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.unidades: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` (`id`, `numero_unidade`, `metragem`, `vagas_garagem`, `data_cadastro`, `from_bloco`, `from_condominio`) VALUES
	(1, 101, 2, 2, '2022-03-29 15:55:17', 1, 1),
	(2, 102, 2, 1, '2022-03-29 15:55:17', 2, 2),
	(3, 103, 4, 1, '2022-03-29 15:55:18', 3, 3);
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;

-- Copiando estrutura para tabela projeto.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_user` varchar(255) NOT NULL DEFAULT '0',
  `usuario` varchar(255) NOT NULL DEFAULT '0',
  `senha` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projeto.usuarios: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome_user`, `usuario`, `senha`) VALUES
	(1, 'Willian', 'will', '202cb962ac59075b964b07152d234b70'),
	(5, 'Willian', 'willian', 'caf1a3dfb505ffed0d024130f58c5cfa'),
	(7, 'Willian', 'williaan', '132'),
	(9, 'Willian', 'will', '123'),
	(10, 'Willian', 'willian', '202cb962ac59075b964b07152d234b70'),
	(11, 'Willian', 'willian', '202cb962ac59075b964b07152d234b70');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Copiando estrutura para view projeto.vw_adm
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_adm` (
	`id` INT(11) NOT NULL,
	`nome_adm` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cnpj` VARCHAR(20) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_bloco
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_bloco` (
	`id` INT(11) NOT NULL,
	`numero_bloco` INT(11) NOT NULL,
	`qt_andares` INT(11) NOT NULL,
	`unidade` INT(11) NOT NULL,
	`nome_condominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`from_condominios` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_condominios
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_condominios` (
	`id` INT(11) NOT NULL,
	`nome_condominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`qt_blocos` INT(11) NOT NULL,
	`logradouro` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`numero` INT(11) NULL,
	`bairro` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`cidade` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`cep` VARCHAR(8) NULL COLLATE 'utf8mb4_general_ci',
	`estado` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`nome_conselho` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`nome_adm` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`from_administradora` INT(11) NOT NULL,
	`from_conselho` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_conselho
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_conselho` (
	`id` INT(11) NOT NULL,
	`nome_conselho` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cpf` VARCHAR(15) NOT NULL COLLATE 'utf8mb4_general_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`telefone` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`funcao` ENUM('Síndico','Sub-Síndico','Contador','Porteiro') NOT NULL COLLATE 'utf8mb4_general_ci',
	`nome_condominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`from_condominio` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_moradores
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_moradores` (
	`id` INT(11) NOT NULL,
	`nome_moradores` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`cpf` VARCHAR(15) NOT NULL COLLATE 'utf8mb4_general_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`telefone` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`numero_unidade` INT(11) NULL,
	`numero_bloco` INT(11) NULL,
	`nome_condominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`from_condominio` INT(11) NOT NULL,
	`from_bloco` INT(11) NOT NULL,
	`from_unidade` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_morador_cond
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_morador_cond` (
	`nome_condominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`totalMoradores` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_unidade
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_unidade` (
	`id` INT(11) NOT NULL,
	`numero_unidade` INT(11) NOT NULL,
	`metragem` INT(11) NOT NULL,
	`vagas_garagem` INT(11) NOT NULL,
	`numero_bloco` INT(11) NULL,
	`nome_condominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`from_bloco` INT(11) NULL,
	`from_condominio` INT(11) NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view projeto.vw_adm
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_adm`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_adm` AS SELECT adm.id, adm.nome_adm, adm.cnpj
FROM administradoras adm ;

-- Copiando estrutura para view projeto.vw_bloco
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_bloco`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_bloco` AS SELECT bloc.id, bloc.numero_bloco, bloc.qt_andares, bloc.unidade, cond.nome_condominio, bloc.from_condominios
FROM blocos bloc
LEFT JOIN condominios cond ON bloc.from_condominios = cond.id ;

-- Copiando estrutura para view projeto.vw_condominios
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_condominios`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_condominios` AS SELECT cond.id, cond.nome_condominio, cond.qt_blocos, cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.cep, cond.estado, cons.nome_conselho, adm.nome_adm, cond.from_administradora, cond.from_conselho
FROM condominios cond
LEFT JOIN conselhos cons ON cond.from_conselho = cons.id
LEFT JOIN administradoras adm ON cond.from_administradora = adm.id ;

-- Copiando estrutura para view projeto.vw_conselho
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_conselho`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_conselho` AS SELECT con.id, con.nome_conselho, con.cpf, con.email, con.telefone, con.funcao, cond.nome_condominio, con.from_condominio
FROM conselhos con
LEFT JOIN condominios cond ON con.from_condominio = cond.id ;

-- Copiando estrutura para view projeto.vw_moradores
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_moradores`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_moradores` AS SELECT morad.id, morad.nome_moradores, morad.cpf, morad.email, morad.telefone, uni.numero_unidade, bloc.numero_bloco, cond.nome_condominio, morad.from_condominio, morad.from_bloco, morad.from_unidade
FROM moradores morad
LEFT JOIN unidades uni ON morad.from_unidade = uni.id
LEFT JOIN blocos bloc ON morad.from_bloco = bloc.id
LEFT JOIN condominios cond ON morad.from_condominio = cond.id ;

-- Copiando estrutura para view projeto.vw_morador_cond
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_morador_cond`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_morador_cond` AS SELECT 
cond.nome_condominio,
COUNT(mor.id) as totalMoradores FROM moradores AS mor
LEFT JOIN condominios AS cond ON cond.id = mor.from_condominio
GROUP BY from_condominio ;

-- Copiando estrutura para view projeto.vw_unidade
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_unidade`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_unidade` AS SELECT unid.id, unid.numero_unidade, unid.metragem, unid.vagas_garagem, bloc.numero_bloco, cond.nome_condominio, unid.from_bloco, unid.from_condominio
FROM unidades unid
LEFT JOIN blocos bloc ON unid.from_bloco = bloc.id
LEFT JOIN condominios cond ON unid.from_condominio = cond.id ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
