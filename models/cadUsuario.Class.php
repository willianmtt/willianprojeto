<?
    class Usuarios extends Dao{
        function __construct(){
            
        }

        function getUsuarios($id){
            $qry = 'SELECT * FROM usuarios';
            if($id){
                $qry .= ' WHERE id ='.$id;
            } 
            return $this->listarData($qry);
        }

        function setUsuarios($dados){
            $values = '';
            $sql = 'INSERT INTO usuarios (';
    
            foreach($dados as $key=>$value){
                $sql .= '`'.$key.'`, ';
                $values .= "'".$value."', ";
            }
            $sql = rtrim($sql,', ');
            $sql .=') VALUES ('.rtrim($values,', ').');';
            return $this->insertData($sql);
        }

        function editUsuarios($dados){
            $sql = 'UPDATE usuarios SET';
    
            foreach($dados as $key=>$value){
                if($key != 'editar'){
                    $sql .= "`".$key."` = '".$value."', ";
                }
            }
            $sql = rtrim($sql,', ');
            $sql .='WHERE ID='.$dados['editar'];
    
            return $this->updateData($sql);
        }

        function deletaUsuarios($id){
            $sql = 'DELETE FROM usuarios WHERE id = '.$id;
            return $this->deletar($sql);
        }
    }
?>