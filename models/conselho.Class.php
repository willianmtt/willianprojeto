<?
Class Conselho extends Unidade{
    protected $id;
    
    function getConselho($id = null){
        $qry = 'SELECT con.id, con.nome_conselho, con.cpf, con.email, con.telefone, con.funcao, cond.nome_condominio, con.from_condominio
        FROM conselhos con
        LEFT JOIN condominios cond ON con.from_condominio = cond.id ';
        if($id){
            $qry .= ' WHERE con.id ='.$id;
            $unique = true;
        } 
        return $this->listarData($qry,$unique,3);
    }

    function setConselho($dados){
        $values = '';
        $sql = 'INSERT INTO conselhos (';

        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }

    function editConselho($dados){
        $sql = 'UPDATE conselhos SET';

        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .='WHERE ID='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaConselho($id){
        $sql = 'DELETE FROM conselhos WHERE id = '.$id;
        return $this->deletar($sql);
    }
}
?>

