<?
Class Administradora extends Condominio{
    protected $id;
    
    function getAdm($id = null){
        $qry = 'SELECT adm.id, adm.nome_adm, adm.cnpj
        FROM administradoras adm ';
        if($id){
            $qry .= ' WHERE adm.id ='.$id;
            $unique = true;
        } 
        return $this->listarData($qry,$unique,3);
    }

    function setAdm($dados){
        $values = '';
        $sql = 'INSERT INTO administradoras (';

        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }

    function editAdm($dados){
        $sql = 'UPDATE administradoras SET';

        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .='WHERE ID='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaAdm($id){
        $sql = 'DELETE FROM administradoras WHERE id = '.$id;
        return $this->deletar($sql);
    }
}
?>