<?
Class Unidade extends Bloco{
    protected $id;
    
    function getUnidades($id = null){
        $qry = 'SELECT unid.id, unid.numero_unidade, unid.metragem, unid.vagas_garagem, bloc.numero_bloco, cond.nome_condominio, unid.from_bloco, unid.from_condominio
        FROM unidades unid
        LEFT JOIN blocos bloc ON unid.from_bloco = bloc.id
        LEFT JOIN condominios cond ON unid.from_condominio = cond.id ';
        if($id){
            $qry .= ' WHERE unid.id ='.$id;
            $unique = true;
        } 
        return $this->listarData($qry,$unique,3);
    }

    function setUnidades($dados){
        $values = '';
        $sql = 'INSERT INTO unidades (';

        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }

    function editUnidades($dados){
        $sql = 'UPDATE unidades SET';

        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .='WHERE ID='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaUnidades($id){
        $sql = 'DELETE FROM unidades WHERE id = '.$id;
        return $this->deletar($sql);
    }

    function getUnidadesFromBloco($id){
        $qry = 'SELECT id, numero_unidade FROM unidades WHERE from_bloco = '.$id;
        return $this->listarData($qry);
    }
}
?>
