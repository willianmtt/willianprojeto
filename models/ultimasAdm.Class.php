<?
    class UltimasAdm extends Dao{
        function getUltimasAdm($id = null){
            $qry = 'SELECT cond.id, cond.nome_condominio, cond.qt_blocos, cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.cep, cond.estado, cons.nome_conselho, adm.nome_adm, cond.from_administradora, cond.from_conselho
            FROM condominios cond
            LEFT JOIN conselhos cons ON cond.from_conselho = cons.id
            LEFT JOIN administradoras adm ON cond.from_administradora = adm.id 
            ORDER BY id DESC LIMIT 5';
            if($id){
                $qry .= ' WHERE cond.id ='.$id;
            } 
            return $this->listarData($qry);
        }

        function getMoradoresCond($id = null){
            $qry = 'SELECT 
            cond.nome_condominio,
            COUNT(mor.id) as totalMoradores 
            FROM moradores AS mor
            LEFT JOIN condominios AS cond 
            ON cond.id = mor.from_condominio
            GROUP BY from_condominio';
            if($id){
                $qry .= ' WHERE id ='.$id;
            } 
            return $this->listarData($qry);
        }
    }
?>