<?
Class Bloco extends Condominio{
    protected $id;
    
    function getBlocos($id = null){
        $qry = 'SELECT bloc.id, bloc.numero_bloco, bloc.qt_andares, bloc.unidade, cond.nome_condominio, bloc.from_condominios
        FROM blocos bloc
        LEFT JOIN condominios cond ON bloc.from_condominios = cond.id ';
        if($id){
            $qry .= ' WHERE bloc.id ='.$id;
            $unique = true;
        } 
        return $this->listarData($qry,$unique,3);
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, numero_bloco FROM blocos WHERE from_condominios = '.$cond;
        return $this->listarData($qry);
    }

    function setBlocos($dados){
        $values = '';
        $sql = 'INSERT INTO blocos (';

        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }

    function editBlocos($dados){
        $sql = 'UPDATE blocos SET';

        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .='WHERE ID='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaBlocos($id){
        $sql = 'DELETE FROM blocos WHERE id = '.$id;
        return $this->deletar($sql);
    }
}
?>