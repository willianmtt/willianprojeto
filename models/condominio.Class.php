<?
Class Condominio extends Dao{
    protected $id;
    
    function getCondominios($id = null){
        $qry = 'SELECT cond.id, cond.nome_condominio, cond.qt_blocos, cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.cep, cond.estado, cons.nome_conselho, adm.nome_adm, cond.from_administradora, cond.from_conselho
        FROM condominios cond
        LEFT JOIN conselhos cons ON cond.from_conselho = cons.id
        LEFT JOIN administradoras adm ON cond.from_administradora = adm.id ';
        if($id){
            $qry .= ' WHERE cond.id ='.$id;
            $unique = true;
        } 
        return $this->listarData($qry,$unique,3);
    }

    function setCondominios($dados){
        $values = '';
        $sql = 'INSERT INTO condominios (';

        foreach($dados as $key=>$value){
            $sql .= '`'.$key.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').');';
        return $this->insertData($sql);
    }

    function editCondominios($dados){
        $sql = 'UPDATE condominios SET';

        foreach($dados as $key=>$value){
            if($key != 'editar'){
                $sql .= "`".$key."` = '".$value."', ";
            }
        }
        $sql = rtrim($sql,', ');
        $sql .='WHERE ID='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaCondominios($id){
        $sql = 'DELETE FROM condominios WHERE id = '.$id;
        return $this->deletar($sql);
    }
}
?>

