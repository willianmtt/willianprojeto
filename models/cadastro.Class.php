<?
class Cadastro extends Unidade
{

    function __construct()
    {
    }

    function getCondominos($id = null)
    {
        $qry = 'SELECT * FROM vw_moradores ';
        $contaTermos = count($this->busca);

        $isNull = false;

        if ($contaTermos > 0) {

            $i = 0;
            foreach ($this->busca as $key => $termo) {
                if ($i == 0 && $termo != null) {

                    $qry = $qry . ' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if (!empty($termo)) {
                            $qry = $qry . $key . ' = ' . $termo . ' AND ';
                        }
                        break;
                    default:
                        if (!empty($termo)) {
                            $qry = $qry . $key . ' LIKE "%' . $termo . '%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE id =' . $id;
            $unique = true;
        }
        return $this->listarData($qry, $unique, 3);
    }

    function setCondominos($dados)
    {
        $values = '';
        $sql = 'INSERT INTO moradores (';

        foreach ($dados as $key => $value) {
            $sql .= '`' . $key . '`, ';
            $values .= "'" . $value . "', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES (' . rtrim($values, ', ') . ');';
        return $this->insertData($sql);
    }

    function editCondominos($dados)
    {
        $sql = 'UPDATE moradores SET';

        foreach ($dados as $key => $value) {
            if ($key != 'editar') {
                $sql .= "`" . $key . "` = '" . $value . "', ";
            }
        }
        $sql = rtrim($sql, ', ');
        $sql .= 'WHERE ID=' . $dados['editar'];

        return $this->updateData($sql);
    }

    function deletaCondominos($id)
    {
        $sql = 'DELETE FROM moradores WHERE id = ' . $id;
        return $this->deletar($sql);
    }
}
