$(function () {
    $('.formCadastro').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/cliente/editaCliente.php'
            urlRedir = url_site+'condominos'
        } else {
            url = url_site+'api/cliente/cadastraCliente.php'
            urlRedir = url_site+'condominos'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })
    
    $('.formCondominio').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/condominio/editaCondominio.php'
            urlRedir = url_site+'listaCondominios'
        } else {
            url = url_site+'api/condominio/cadastraCondominio.php'
            urlRedir = url_site+'listaCondominios'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })

    $('.formUnidades').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/unidade/editaUnidade.php'
            urlRedir = url_site+'listaUnidades'
        } else {
            url = url_site+'api/unidade/cadastraUnidade.php'
            urlRedir = url_site+'listaUnidades'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })
    $('.formBloco').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/bloco/editaBloco.php'
            urlRedir = url_site+'listaBlocos'
        } else {
            url = url_site+'api/bloco/cadastraBloco.php'
            urlRedir = url_site+'listaBlocos'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })

    $('.formConselho').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/conselho/editaConselho.php'
            urlRedir = url_site+'listaConselho'
        } else {
            url = url_site+'api/conselho/cadastraConselho.php'
            urlRedir = url_site+'listaConselho'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })

    $('.formAdm').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/administradora/editaAdm.php'
            urlRedir = url_site+'listaAdministradora'
        } else {
            url = url_site+'api/administradora/cadastraAdm.php'
            urlRedir = url_site+'listaAdministradora'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })

    $('.cadUser').submit(function () {
        let editar = $(this).find('input[name="editar"]').val()
        let url
        let urlRedir

        if (editar) {
            url = url_site+'api/usuarios/editaUser.php'
            urlRedir = url_site+'listaCadUsuario'
        } else {
            url = url_site+'api/usuarios/cadastraUser.php'
            urlRedir = url_site+'listaCadUsuario'
        }
        $('.botaozin').attr('disabled', true)
        
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    // $('.totalRegistros').html('Total Registros: ' + data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir)
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }
            }
        })
        return false;
    })


    //deletar
    $('#listaClientes').on('click', '.removerCliente', function () {
        let idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/cliente/deletaCliente.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'condominos')
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    })

    $('#listaCondominios').on('click', '.removerCondominios', function () {
        let idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/condominio/deletaCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaCondominios')
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    })

    $('#listaUnidade').on('click', '.removerUnidades', function () {
        let idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/unidade/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaUnidades')
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    })

    $('#listaBlocos').on('click', '.removerBlocos', function () {
        let idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/bloco/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaBlocos')
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    })

    $('#listaConselho').on('click', '.removerConselho', function () {
        let idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/conselho/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaConselho')
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    })

    $('#listaUsuarios').on('click', '.removerUsuario', function () {
        let idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/usuarios/deletaUser.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaCadUsuario')
                } else {
                    myAlert(data.status, data.msg, 'main')
                }
            }
        })
        return false;
    })


    $(".cadUser").submit(function(){
        var senha = $(this).find('.senha').val();
        var confirmaSenha = $(this).find('.csenha').val();

        if(senha == confirmaSenha){
            $.ajax({
                url: url_site+'api/usuarios/cadastraUser.php',
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data){
                    if(data.status == 'success'){
                        myAlert(data.status, data.msg, 'main');
                    }else{
                        myAlert(data.status, data.msg, 'main');
                    }
                }
            })
        }else{
            document.myAlert('danger','A senha digitada no campo de confirmação não confere!', 'main');
        }

        return false;
    })

    // chamar valores para select
    $('.fromCondominio').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: url_site+'api/bloco/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: {id:selecionado},
            success: function(data){
                selectPopulation('.fromBloco',data.resultSet,'numero_bloco');
            }
        })
    })
    //chamar unidades
    $('.fromBloco').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: url_site+'api/unidade/listUnidades.php',
            dataType: 'json',
            type: 'post',
            data: {id: selecionado},
            success: function(data){
                selectPopulation('.fromUnidade', data.resultSet, 'numero_unidade');
            }
        })
    })

    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione...</option>';

            for(let i = 0; i < dados.length; i++){
                estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
        }
        $(seletor).html(estrutura)
    }

    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : ''

        window.location.href = url_site+pagina+'/busca/'+termo1+termo2

        return false;
    })

    $('.termo1, .termo2').on('keyup focusout change', function(){
        let termo1 = $('.termo1').val();
        let termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled', false);
        }else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })
})

function myAlert(tipo, mensagem, pai, url) {
    url = (url == undefined) ? url == '' : url
    componente = '<div class="alert alert-' + tipo + '" role="alert">' + mensagem + '</div>'

    $(pai).prepend(componente)

    setTimeout(function () {
        $(pai).find('div.alert').remove();
        //redirecionar
        if(tipo == 'success' && url){
            setTimeout(function() {
                window.location.href = url
            }, 200)
        }
    }, 1500)

}

// $(document).ready(function(){
//     $('.cep').mask('00000-000');
//     $('.phone').mask('0000-0000');
//     $('.cpf').mask('000.000.000-00', {reverse: true});
//     $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
//   });